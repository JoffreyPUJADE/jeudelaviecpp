#include "../include/Grille.hpp"

#include <ostream>
#include <istream>
#include <sstream>
#include <string>
#include <vector>
#include <stdexcept>
#include <SFML/Graphics.hpp>

Grille::Grille(uint taille, std::vector<std::string> tabConfig)
{
	m_taille = taille;
	
	m_longueurCase = 15;
	m_largeurCase = 15;
	
	if(tabConfig.size() != m_taille)
		throw std::runtime_error("ERREUR : Nombre de lignes du tableau de configuration different a la taille donnee.");
	
	for(size_t i=0;i<tabConfig.size();++i)
	{
		if(tabConfig[i].length() != m_taille)
		{
			std::ostringstream ossErr;
			
			ossErr << "ERREUR : La ligne " << (i + 1) << " a une longueur differente de la taille donnee.";
			
			throw std::runtime_error(ossErr.str().c_str());
		}
		
		for(size_t j=0;j<tabConfig[i].length();++j)
		{
			if(tabConfig[i][j] == '0')
				m_cases.push_back(Case(((j + 1) * m_longueurCase), ((i + 1) * m_largeurCase), m_longueurCase, m_largeurCase, Couleur::Noir));
			else if(tabConfig[i][j] == '1')
				m_cases.push_back(Case(((j + 1) * m_longueurCase), ((i + 1) * m_largeurCase), m_longueurCase, m_largeurCase, Couleur::Blanc));
			else
			{
				std::ostringstream ossErr;
				
				ossErr << "ERREUR : '" << tabConfig[i][j] << "' n'est pas une couleur de configuration valide.";
				
				throw std::runtime_error(ossErr.str().c_str());
			}
		}
	}
}

Grille::Grille(std::istream& is, bool config)
{
	if(config)
	{
		is >> m_taille;
		
		uint m_longueurCase = 15;
		uint m_largeurCase = 15;
		
		std::string ligne;
		std::string tampon;
		uint compteur = 0;
		
		std::getline(is, tampon);
		
		while(std::getline(is, ligne))
		{
			if(ligne.length() != m_taille)
			{
				std::ostringstream ossErr;
				
				ossErr << "ERREUR : La ligne " << (compteur + 1) << " a une longueur differente de la taille donnee.";
				
				throw std::runtime_error(ossErr.str().c_str());
			}
			
			for(size_t j=0;j<ligne.length();++j)
			{
				if(ligne[j] == '0')
					m_cases.push_back(Case((j * m_longueurCase), (compteur * m_largeurCase), m_longueurCase, m_largeurCase, Couleur::Noir));
				else if(ligne[j] == '1')
					m_cases.push_back(Case((j * m_longueurCase), (compteur * m_largeurCase), m_longueurCase, m_largeurCase, Couleur::Blanc));
				else
				{
					std::ostringstream ossErr;
					
					ossErr << "ERREUR : '" << ligne[j] << "' n'est pas une couleur de configuration valide.";
					
					throw std::runtime_error(ossErr.str().c_str());
				}
			}
			
			++compteur;
		}
		
		if(compteur != m_taille)
			throw std::runtime_error("ERREUR : Nombre de lignes du fichier de configuration different a la taille donnee.");
	}
	else
	{
		is >> m_taille;
		
		for(size_t i=0;i<(m_taille*m_taille);++i)
		{
			m_cases.push_back(Case(is));
		}
	}
}

Grille::Grille(Grille const& src) : m_taille(src.m_taille), m_cases(src.m_cases) {}

Grille::~Grille() {}


Grille& Grille::operator=(Grille const& src)
{
	m_taille = src.m_taille;
	m_cases = src.m_cases;
	
	return *this;
}

Case Grille::operator[](size_t indice) const
{
	if((static_cast<int>(indice) < 0) || (indice >= m_cases.size()))
		throw std::range_error("ERREUR : Indice invalide.");
	
	return m_cases[indice];
}

Case& Grille::operator[](size_t indice)
{
	if((static_cast<int>(indice) < 0) || (indice >= m_cases.size()))
		throw std::range_error("ERREUR : Indice invalide.");
	
	return m_cases[indice];
}


Case Grille::operator()(size_t x, size_t y) const
{
	if((static_cast<int>(x) < 0) || (x >= m_cases.size()))
		throw std::range_error("ERREUR : Position x invalide.");
	
	if((static_cast<int>(y) < 0) || (y >= m_cases.size()))
		throw std::range_error("ERREUR : Position y invalide.");
	
	if((static_cast<int>((y * m_taille + x)) < 0) || ((y * m_taille + x) >= m_cases.size()))
		throw std::range_error("ERREUR : Au moins l'une des positions, x ou y, invalide.");
	
	return m_cases[(y * m_taille + x)];
}

Case& Grille::operator()(size_t x, size_t y)
{
	if((static_cast<int>(x) < 0) || (x >= m_cases.size()))
		throw std::range_error("ERREUR : Position x invalide.");
	
	if((static_cast<int>(y) < 0) || (y >= m_cases.size()))
		throw std::range_error("ERREUR : Position y invalide.");
	
	if((static_cast<int>((y * m_taille + x)) < 0) || ((y * m_taille + x) >= m_cases.size()))
		throw std::range_error("ERREUR : Au moins l'une des positions, x ou y, invalide.");
	
	return m_cases[(y * m_taille + x)];
}


void Grille::setLongueurCase(uint longueurCase)
{
	m_longueurCase = longueurCase;
	
	for(size_t i=0;i<m_cases.size();++i)
		m_cases[i].setLongueur(m_longueurCase);
}

void Grille::setLargeurCase(uint largeurCase)
{
	m_largeurCase = largeurCase;
	
	for(size_t i=0;i<m_cases.size();++i)
		m_cases[i].setLargeur(m_largeurCase);
}

void Grille::setX(int x)
{
	std::vector<int> tabX;
	
	for(size_t i=0;i<m_taille;++i)
	{
		for(size_t j=0;j<m_taille;++j)
			tabX.push_back(m_cases[(i * m_taille + j)].getX());
	}
	
	for(size_t i=0;i<m_taille;++i)
	{
		for(size_t j=0;j<m_taille;++j)
			m_cases[(i * m_taille + j)].moveToX(((x + (i > 0 ? tabX[((i - 1) * m_taille + j)] : 0)) - m_cases[(i * m_taille + j)].getX()));
	}
}

void Grille::setY(int y)
{
	std::vector<int> tabY;
	
	for(size_t i=0;i<m_taille;++i)
	{
		for(size_t j=0;j<m_taille;++j)
			tabY.push_back(m_cases[(i * m_taille + j)].getY());
	}
	
	for(size_t i=0;i<m_taille;++i)
	{
		for(size_t j=0;j<m_taille;++j)
			m_cases[(i * m_taille + j)].moveToY(((y + (i > 0 ? tabY[((i - 1) * m_taille + j)] : 0)) - m_cases[(i * m_taille + j)].getY()));
	}
}


std::vector<Case> Grille::voisins(size_t x, size_t y) const
{
	std::vector<Case> tabRes;
	llint xLlint = static_cast<llint>(x);
	llint yLlint = static_cast<llint>(y);
	
	for(llint i=(yLlint-1);i<(yLlint+2);++i)
	{
		for(llint j=(xLlint-1);j<(xLlint+2);++j)
		{
			bool yDedans = ((i >= 0) && (i < static_cast<llint>(m_taille)));
			bool xDedans = ((j >= 0) && (j < static_cast<llint>(m_taille)));
			
			if(!((i == yLlint) && (j == xLlint))) // Si on est pas sur le point courant, alors...
			{
				if(yDedans && xDedans) // Si y et x ne sont pas en-dehors de la grille, alors...
					tabRes.push_back(m_cases[(i * m_taille + j)]);
			}
		}
	}
	
	return tabRes;
}

void Grille::dessiner(sf::RenderWindow& fenetre) const
{
	for(size_t i=0;i<m_cases.size();++i)
		m_cases[i].dessiner(fenetre);
}

std::vector<Position> Grille::indicesVoisins(size_t x, size_t y)
{
	std::vector<Position> tabRes;
	llint xLlint = static_cast<llint>(x);
	llint yLlint = static_cast<llint>(y);
	
	for(llint i=(yLlint-1);i<(yLlint+2);++i)
	{
		for(llint j=(xLlint-1);j<(xLlint+2);++j)
		{
			bool yDedans = ((i >= 0) && (i < static_cast<llint>(m_taille)));
			bool xDedans = ((j >= 0) && (j < static_cast<llint>(m_taille)));
			
			if(!((i == yLlint) && (j == xLlint))) // Si on est pas sur le point courant, alors...
			{
				if(yDedans && xDedans) // Si y et x ne sont pas en-dehors de la grille, alors...
					tabRes.push_back({static_cast<size_t>(j), static_cast<size_t>(i)});
			}
		}
	}
	
	return tabRes;
}

void Grille::testVoisins()
{
	std::vector<Position> tabPositions = indicesVoisins(0, 0);
	
	for(size_t i=0;i<tabPositions.size();++i)
		m_cases[(tabPositions[i].y * m_taille + tabPositions[i].x)].setCouleur(Couleur::Vert);
	
	std::vector<Position> tabPositions2 = indicesVoisins(0, 49);
	
	for(size_t i=0;i<tabPositions2.size();++i)
		m_cases[(tabPositions2[i].y * m_taille + tabPositions2[i].x)].setCouleur(Couleur::Bleu);
	
	std::vector<Position> tabPositions3 = indicesVoisins(49, 0);
	
	for(size_t i=0;i<tabPositions3.size();++i)
		m_cases[(tabPositions3[i].y * m_taille + tabPositions3[i].x)].setCouleur(Couleur::Jaune);
	
	std::vector<Position> tabPositions4 = indicesVoisins(49, 49);
	
	for(size_t i=0;i<tabPositions4.size();++i)
		m_cases[(tabPositions4[i].y * m_taille + tabPositions4[i].x)].setCouleur(Couleur::Magenta);
	
	std::vector<Position> tabPositions5 = indicesVoisins(24, 24);
	
	for(size_t i=0;i<tabPositions5.size();++i)
		m_cases[(tabPositions5[i].y * m_taille + tabPositions5[i].x)].setCouleur(Couleur::Cyan);
	
	std::vector<Position> tabPositions6 = indicesVoisins(35, 35);
	
	for(size_t i=0;i<tabPositions6.size();++i)
		m_cases[(tabPositions6[i].y * m_taille + tabPositions6[i].x)].setCouleur(Couleur::Vert);
}


std::ostream& operator<<(std::ostream& os, Grille const& src)
{
	src.ecrire(os);
	
	return os;
}

std::istream& operator>>(std::istream& is, Grille& src)
{
	src.lire(is);
	
	return is;
}


void Grille::ecrire(std::ostream& os) const
{
	os << m_taille << "\n";
	
	for(size_t i=0;i<m_cases.size();++i)
		os << m_cases[i] << ((i < (m_cases.size() - 1)) ? "\n" : "");
}

void Grille::lire(std::istream& is)
{
	is >> m_taille;
	
	m_cases = std::vector<Case>();
	
	for(size_t i=0;i<(m_taille*m_taille);++i)
		m_cases.push_back(Case(is));
}