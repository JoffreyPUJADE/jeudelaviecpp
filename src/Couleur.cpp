#include "../include/Couleur.hpp"

#include <ostream>
#include <istream>
#include <sstream>
#include <string>
#include <regex>
#include <math.h>
#include <cmath>
#include <stdexcept>
#include <SFML/Graphics.hpp>

// Fonction template qui renvoie la valeur maximale entre deux valeurs.
template<typename T>
bool max(T v1, T v2)
{
	return ((v1 < v2) ? v2 : v1);
}

// Fonction variadique template qui renvoie la valeur maximale entre X valeurs.
template<typename T, typename... Args>
bool max(T premier, Args... args)
{
	return max<T>(premier, args...);
}

// Fonction template qui renvoie la valeur minimale entre deux valeurs.
template<typename T>
bool min(T v1, T v2)
{
	return ((v1 < v2) ? v1 : v2);
}

// Fonction variadique template qui renvoie la valeur minimale entre X valeurs.
template<typename T, typename... Args>
bool min(T premier, Args... args)
{
	return min<T>(premier, args...);
}

// Fonction template qui converti un type vers un autre pour une valeur donnée.
template<typename TypeSource, typename TypeCible>
TypeCible valeurVersType(TypeSource val)
{
	std::ostringstream ossConvert;
	
	ossConvert << val;
	
	TypeCible res = 0;
	
	std::istringstream issConvert(ossConvert.str());
	
	issConvert >> res;
	
	return res;
}

// Fonction template qui renvoie la lettre hexadécimale correspondant à un nombre décimal supérieur ou égal à 10.
template<typename T>
std::string hexLetter(T decimalValue)
{
	int decVal = valeurVersType<T, int>(decimalValue);
	
	std::ostringstream oss;
	
	switch(decVal)
	{
		case 0 :
			oss << "0";
		break;
		
		case 10 :
			oss << "A";
		break;
		
		case 11 :
			oss << "B";
		break;
		
		case 12 :
			oss << "C";
		break;
		
		case 13 :
			oss << "D";
		break;
		
		case 14 :
			oss << "E";
		break;
		
		case 15 :
			oss << "F";
		break;
		
		default :
			oss << decVal;
		break;
	}
	
	return oss.str();
}

// Fonction template qui converti un nombre décimal en un nombre hexadécimal.
// /!\ ATTENTION /!\ : Cette conversion est adaptée pour les couleurs.
template<typename T>
std::string dec2Hex(T decimalValue)
{
	std::ostringstream oss;
	
	int decVal = static_cast<int>(decimalValue);
	
	if(decVal == 0)
	{
		oss << "00";
	}
	
	while(decVal > 0)
	{
		int valTemp = (decVal % 16);
		decVal /= 16;
		
		oss << hexLetter<int>(valTemp);
	}
	
	return oss.str();
}

// Fonction template qui renvoie le nombre décimal correspondant à un nombre hexadécimal.
template<typename T>
T hexLetter2Dec(char hexLet)
{
	switch(hexLet)
	{
		case 'A' :
			return 10;
		break;
		
		case 'B' :
			return 11;
		break;
		
		case 'C' :
			return 12;
		break;
		
		case 'D' :
			return 13;
		break;
		
		case 'E' :
			return 14;
		break;
		
		case 'F' :
			return 15;
		break;
		
		default:
			return (hexLet - '0');
		break;
	}
}

// Fonction template qui converti un nombre hexadécimal en un nombre décimal.
// /!\ ATTENTION /!\ : Cette conversion est adaptée pour les couleurs.
template<typename T>
T hex2Digits2Dec(std::string hexDigits)
{
	int resInt = 0;
	
	resInt += (hexLetter2Dec<int>(hexDigits[0]) * 16);
	resInt += (hexLetter2Dec<int>(hexDigits[1]) * 16);
	
	return valeurVersType<int, T>(resInt);
}

// Fonction template qui renvoie la valeur absolue d'un nombre.
template<typename T>
T valeurAbsolue(T valeur)
{
	return ((valeur < 0) ? -valeur : valeur);
}

Couleur Couleur::Rouge = Couleur(255, 0, 0, 255);
Couleur Couleur::Vert = Couleur(0, 255, 0, 255);
Couleur Couleur::Bleu = Couleur(0, 0, 255, 255);
Couleur Couleur::Jaune = Couleur(255, 255, 0, 255);
Couleur Couleur::Magenta = Couleur(255, 0, 255, 255);
Couleur Couleur::Cyan = Couleur(0, 255, 255, 255);
Couleur Couleur::Noir = Couleur(0, 0, 0, 255);
Couleur Couleur::Blanc = Couleur(255, 255, 255, 255);
Couleur Couleur::Transparent = Couleur(0, 0, 0, 0);

Couleur::Couleur() : m_rouge(255), m_vert(255), m_bleu(255), m_transparent(255) {}

Couleur::Couleur(int r, int g, int b, int alpha) : m_rouge(r), m_vert(g), m_bleu(b), m_transparent(alpha) {}

Couleur::Couleur(float H, float S, float L) : m_rouge(0), m_vert(0), m_bleu(0), m_transparent(255)
{
	float C = (1.0 - abs(((2 * L) - 1.0))) * S;
	float X = C * (1.0 - valeurAbsolue<float>((fmod((H / 60.0), 2.0) - 1.0)));
	float m = L - C/2.0;
	
	float r_ = 0.0;
	float g_ = 0.0;
	float b_ = 0.0;
	
	if((H >= 0.0) && (H < 60.0))
	{
		r_ = C;
		g_ = X;
		b_ = 0.0;
	}
	else if((H >= 60.0) && (H < 120.0))
	{
		r_ = X;
		g_ = C;
		b_ = 0.0;
	}
	else if((H >= 120.0) && (H < 180.0))
	{
		r_ = 0.0;
		g_ = C;
		b_ = X;
	}
	else if((H >= 180.0) && (H < 240.0))
	{
		r_ = 0.0;
		g_ = X;
		b_ = C;
	}
	else if((H >= 240.0) && (H < 300.0))
	{
		r_ = X;
		g_ = 0.0;
		b_ = C;
	}
	else if((H >= 300.0) && (H < 360.0))
	{
		r_ = C;
		g_ = 0.0;
		b_ = X;
	}
	
	m_rouge = (r_ + m) * 255;
	m_vert = (g_ + m) * 255;
	m_bleu = (b_ + m) * 255;
}

Couleur::Couleur(HSL hsl) : Couleur(hsl.H, hsl.S, hsl.L) {}

Couleur::Couleur(std::string hexColor) : m_rouge(0), m_vert(0), m_bleu(0), m_transparent(255)
{
	if((hexColor.length() != 6) && (hexColor.length() != 7))
		throw std::range_error("ERREUR : La couleur hexadecimale doit etre de 6 ou 7 caracteres !");
	
	if(hexColor.length() == 7)
	{
		std::regex motif("#[0-9][0-9][0-9][0-9][0-9][0-9]");
		
		if(!std::regex_match(hexColor, motif))
			throw std::runtime_error("ERREUR : Le motif de la couleur doit etre de la forme : #000000 !");
		
		hexColor = hexColor.substr(1);
	}
	
	m_rouge = hex2Digits2Dec<int>(hexColor.substr(0, 2));
	m_vert = hex2Digits2Dec<int>(hexColor.substr(2, 2));
	m_bleu = hex2Digits2Dec<int>(hexColor.substr(4, 2));
}

Couleur::Couleur(std::istream& is)
{
	is >> m_rouge;
	is >> m_vert;
	is >> m_bleu;
	is >> m_transparent;
}

Couleur::Couleur(Couleur const& src) : Couleur(src.m_rouge, src.m_vert, src.m_bleu, src.m_transparent) {}

Couleur::~Couleur() {}


Couleur& Couleur::operator=(Couleur const& src)
{
	m_rouge = src.m_rouge;
	m_vert = src.m_vert;
	m_bleu = src.m_bleu;
	m_transparent = src.m_transparent;
	
	return *this;
}


HSL Couleur::toHSL() const
{
	float r_ = (static_cast<float>(m_rouge) / 255.0);
	float g_ = (static_cast<float>(m_vert) / 255.0);
	float b_ = (static_cast<float>(m_bleu) / 255.0);
	
	float Cmax = max(r_, g_, b_);
	float Cmin = min(r_, g_, b_);
	
	float delta = (Cmax - Cmin);
	
	float H = 0.0;
	float S = 0.0;
	float L = 0.0;
	
	// Calcul de H, la valeur de "Teinte".
	
	if(delta == 0.0)
	{
		H = 0.0;
	}
	else if(Cmax == r_)
	{
		H = 60.0 * (fmod(((g_ - b_) / delta), 6.0));
	}
	else if(Cmax == g_)
	{
		H = 60.0 * (((b_ - r_) / delta) + 2.0);
	}
	else if(Cmax == b_)
	{
		H = 60.0 * (((r_ - g_) / delta) + 4.0);
	}
	
	// Calcul de L, la valeur de "Luminance".
	
	L = (Cmax + Cmin) / 2.0;
	
	// Calcul de S, la valeur de "Saturation".
	
	if(delta == 0)
	{
		S = 0.0;
	}
	else if((delta < 0.0) || (delta > 0.0))
	{
		S = (delta / (1.0 - valeurAbsolue<float>(((2.0 * L) - 1.0))));
	}
	
	return HSL(H, S, L);
}

std::string Couleur::toHex() const
{
	std::ostringstream oss;
	
	oss << dec2Hex<int>(m_rouge) << dec2Hex<int>(m_vert) << dec2Hex<int>(m_bleu);
	
	return oss.str();
}


std::ostream& operator<<(std::ostream& os, Couleur const& src)
{
	src.ecrire(os);
	
	return os;
}

std::istream& operator>>(std::istream& is, Couleur& src)
{
	src.lire(is);
	
	return is;
}

bool operator==(Couleur const& src1, Couleur const& src2)
{
	return src1.estEgal(src2);
}

bool operator!=(Couleur const& src1, Couleur const& src2)
{
	return !src1.estEgal(src2);
}


void Couleur::ecrire(std::ostream& os) const
{
	os << "(" << m_rouge << ", " << m_vert << ", " << m_bleu << ", " << m_transparent << ")";
}

void Couleur::lire(std::istream& is)
{
	is >> m_rouge;
	is >> m_vert;
	is >> m_bleu;
	is >> m_transparent;
}

bool Couleur::estEgal(Couleur const& src) const
{
	return ((m_rouge == src.m_rouge) && (m_vert == src.m_vert) && (m_bleu == src.m_bleu) && (m_transparent == src.m_transparent));
}