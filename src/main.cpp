#include "../include/Inclusions.hpp"
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdexcept>

void takeScreenshot(const sf::RenderWindow& window, std::string const& fileName)
{
    sf::Texture texture;
    texture.create(window.getSize().x, window.getSize().y);
    texture.update(window);
    if(texture.copyToImage().saveToFile(fileName))
    {
        std::cout << "screenshot saved to " << fileName << std::endl;
    }
    else
    {
		throw std::runtime_error("ERREUR SCREENSHOT");
    }
}

Grille creerGrilleAvecFichier(char const* nomFichier)
{
	std::ifstream ifs;
	
	ifs.open(nomFichier);
	
	if(!ifs.is_open())
	{
		std::ostringstream ossErr;
		
		ossErr << "ERREUR : Fichier \"" << nomFichier << "\" impossible a ouvrir.";
		
		throw std::runtime_error(ossErr.str().c_str());
	}
	
	Grille g(ifs, true);
	
	ifs.close();
	
	return g;
}

int main()
{
	INITIALISATION_ALEATOIRE;
	
	Jeu jeuDeLaVie("config1.txt");
	
	uint longueurFenetre = jeuDeLaVie.getLongueur() + 50;
	uint largeurFenetre = jeuDeLaVie.getLargeur() + 50;
	
	sf::RenderWindow fenetre(sf::VideoMode(longueurFenetre + 2, largeurFenetre + 2), "Jeu de la vie", sf::Style::Default);
	
    while(fenetre.isOpen())
    {
        sf::Event event;
        
        while(fenetre.pollEvent(event))
        {
            switch(event.type)
            {
                case sf::Event::Closed :
                    fenetre.close();
                    break;
				
				case sf::Event::Resized :
				{
					sf::FloatRect visibleArea(0, 0, event.size.width, event.size.height);
					fenetre.setView(sf::View(visibleArea));
				}
				break;
				
				case sf::Event::KeyPressed :
                {
                    switch(event.key.code)
                    {
						case sf::Keyboard::P :
							takeScreenshot(fenetre, "screenshot-" + T2str<long long unsigned int>(nombreAleatoire<long long unsigned int>(1, 150000000)) + ".png");
						break;
						
						case sf::Keyboard::S :
							jeuDeLaVie.tour();
						break;
                        
                        default :
                            break;
                    }
                }
                break;
                
                default :
                    break;
            }
        }
        
        fenetre.clear(sf::Color::White);
		
		jeuDeLaVie.dessiner(fenetre);
        
        fenetre.display();
    }
	
	return 0;
}