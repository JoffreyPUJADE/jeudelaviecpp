#include "../include/Jeu.hpp"

#include "../include/GeneralTemplates.hpp"
#include <ostream>
#include <istream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdexcept>
#include <SFML/Graphics.hpp>

Jeu::Jeu(uint taille, std::vector<std::string> tabConfig) : m_tour(0), m_yIncrementTexte(50), m_grille(taille, tabConfig)
{
	m_grille.setY(m_yIncrementTexte);
}

Jeu::Jeu(Grille grille) : m_tour(0), m_yIncrementTexte(50), m_grille(grille)
{
	m_grille.setY(m_yIncrementTexte);
}

Jeu::Jeu(char const* nomFichier) : m_tour(0), m_yIncrementTexte(50), m_grille(0, std::vector<std::string>())
{
	std::ifstream ifs;
	
	ifs.open(nomFichier);
	
	if(!ifs.is_open())
	{
		std::ostringstream ossErr;
		
		ossErr << "ERREUR : Fichier \"" << nomFichier << "\" impossible a ouvrir.";
		
		throw std::runtime_error(ossErr.str().c_str());
	}
	
	Grille g(ifs, true);
	
	ifs.close();
	
	m_grille = g;
	
	m_grille.setY(m_yIncrementTexte);
}

Jeu::Jeu(std::string nomFichier) : Jeu(nomFichier.c_str()) {}

Jeu::Jeu(Jeu const& src) : Jeu(src.m_grille)
{
	m_tour = src.m_tour;
	m_yIncrementTexte = src.m_yIncrementTexte;
}

Jeu::~Jeu() {}


Jeu& Jeu::operator=(Jeu const& src)
{
	m_tour = src.m_tour;
	m_yIncrementTexte = src.m_yIncrementTexte;
	m_grille = src.m_grille;
	
	return *this;
}


void Jeu::tour()
{
	// Calcul de l'état suivant de chacune des cellules (cases) de la grille.
	for(uint i=0;i<m_grille.getTaille();++i)
	{
		for(uint j=0;j<m_grille.getTaille();++j)
		{
			Case& celluleCourante = m_grille(j, i);
			std::vector<Case> tabVoisins = m_grille.voisins(j, i);
			
			if(celluleCourante.getEtatActuel()) // Si la cellule courante est vivante, alors...
			{
				size_t nbVoisinsVivants = 0;
				
				for(size_t k=0;k<tabVoisins.size();++k)
					nbVoisinsVivants += (tabVoisins[k].getEtatActuel() ? 1 : 0);
				
				celluleCourante.setEtatSuivant(((nbVoisinsVivants == 2) || (nbVoisinsVivants == 3))); // La cellule courante sera vivante au tour prochain si et seulement si elle a 2 ou 3 voisines.
			}
			else // Sinon, si la cellule courante est morte, alors...
			{
				size_t nbVoisinsVivants = 0;
				
				for(size_t k=0;k<tabVoisins.size();++k)
					nbVoisinsVivants += (tabVoisins[k].getEtatActuel() ? 1 : 0);
				
				celluleCourante.setEtatSuivant(nbVoisinsVivants == 3); // La cellule courante sera vivante au tour prochain si et seulement si elle a exactement 3 voisines.
			}
		}
	}
	
	// Basculement vers le prochain tour.
	for(uint i=0;i<m_grille.getTaille();++i)
	{
		for(uint j=0;j<m_grille.getTaille();++j)
		{
			Case& celluleCourante = m_grille(j, i);
			
			celluleCourante.setEtatActuel(celluleCourante.getEtatSuivant());
		}
	}
	
	++m_tour;
}

void Jeu::dessiner(sf::RenderWindow& fenetre) const
{
	std::string texteTourStr = ("Tour : " + T2str<uint>(m_tour));
	
	sf::Font police;
	
	if(!police.loadFromFile("OpenSans-VariableFont_wdth,wght.ttf"))
		throw std::runtime_error("ERREUR : Impossible de charger la police \"OpenSans-VariableFont_wdth,wght.ttf\".");
	
	sf::Text texteTour;
	
	texteTour.setFont(police);
	texteTour.setString(texteTourStr);
	texteTour.setCharacterSize(12);
	texteTour.setFillColor(Couleur::Noir.toSFML());
	texteTour.setPosition(m_grille.getX(), m_grille.getY() - m_yIncrementTexte);
	fenetre.draw(texteTour);
	
	m_grille.dessiner(fenetre);
}


std::ostream& operator<<(std::ostream& os, Jeu const& src)
{
	src.ecrire(os);
	
	return os;
}

std::istream& operator>>(std::istream& is, Jeu& src)
{
	src.lire(is);
	
	return is;
}


void Jeu::ecrire(std::ostream& os) const
{
	os << m_grille;
}

void Jeu::lire(std::istream& is)
{
	is >> m_grille;
}