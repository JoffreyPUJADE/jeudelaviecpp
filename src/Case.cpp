#include "../include/Case.hpp"

#include <ostream>
#include <istream>
#include <SFML/Graphics.hpp>

Case::Case(int x, int y, uint longueur, uint largeur, Couleur couleur) : m_x(x), m_y(y), m_longueur(longueur), m_largeur(largeur), m_actuel(couleur == Couleur::Noir), m_suivant(false), m_couleur(couleur), m_couleurBordure(Couleur::Rouge)
{
	m_actuel = m_couleur == Couleur::Noir;
}

Case::Case(std::istream& is)
{
	is >> m_x;
	is >> m_y;
	is >> m_longueur;
	is >> m_largeur;
	is >> m_actuel;
	is >> m_suivant;
	is >> m_couleur;
	is >> m_couleurBordure;
}

Case::Case(Case const& src) : Case(src.m_x, src.m_y, src.m_longueur, src.m_largeur, src.m_couleur) {}

Case::~Case() {}


Case& Case::operator=(Case const& src)
{
	m_x = src.m_x;
	m_y = src.m_y;
	m_longueur = src.m_longueur;
	m_largeur = src.m_largeur;
	m_actuel = src.m_actuel;
	m_suivant = src.m_suivant;
	m_couleur = src.m_couleur;
	m_couleurBordure = src.m_couleurBordure;
	
	return *this;
}


void Case::dessiner(sf::RenderWindow& fenetre) const
{
	sf::RectangleShape rect(sf::Vector2f(m_longueur, m_largeur));
	
	Couleur c = m_actuel ? Couleur::Noir : Couleur::Blanc;
	rect.setFillColor(c.toSFML());
	rect.setOutlineColor(m_couleurBordure.toSFML());
	rect.setOutlineThickness(1.0);
	rect.setPosition(m_x, m_y);
	
	fenetre.draw(rect);
}


std::ostream& operator<<(std::ostream& os, Case const& src)
{
	src.ecrire(os);
	
	return os;
}

std::istream& operator>>(std::istream& is, Case& src)
{
	src.lire(is);
	
	return is;
}


void Case::ecrire(std::ostream& os) const
{
	os << m_x << " " << m_y << " " << m_longueur << " " << m_largeur << " " << m_couleur << " " << m_couleurBordure;
}

void Case::lire(std::istream& is)
{
	is >> m_x;
	is >> m_y;
	is >> m_longueur;
	is >> m_largeur;
	is >> m_couleur;
	is >> m_couleurBordure;
}