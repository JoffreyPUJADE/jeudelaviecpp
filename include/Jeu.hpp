#ifndef DEF_JEU
#define DEF_JEU

#include "Case.hpp"
#include "Grille.hpp"
#include <ostream>
#include <istream>
#include <string>
#include <vector>
#include <SFML/Graphics.hpp>

typedef unsigned int uint;

class Jeu
{
	public:
		Jeu(uint taille, std::vector<std::string> tabConfig);
		Jeu(Grille grille);
		Jeu(char const* nomFichier);
		Jeu(std::string nomFichier);
		Jeu(Jeu const& src);
		~Jeu();
		
		/*! \brief Surcharge de l'opérateur d'affectation.
		 * \param [inout] src (Jeu const&) : Instance à recopier.
		 * \details Cette surcharge de l'opérateur d'affectation permet d'affecter de manière "sécurisée" les caractéristiques
		 * 			de l'instance passée en paramètre à l'instance courante.
		 * \return Référence sur l'instance courante (Jeu&).
		 * \callergraph
		 * \callgraph
		 */
		Jeu& operator=(Jeu const& src);
		
		inline Grille getGrille() const { return m_grille; }
		inline uint getLongueur() const { return (m_grille.getLongueur() + m_yIncrementTexte); }
		inline uint getLargeur() const { return m_grille.getLargeur(); }
		
		inline void setGrille(Grille grille) { m_grille = grille; }
		
		void tour();
		void dessiner(sf::RenderWindow& fenetre) const;
		
		
		/*! \brief Surcharge de l'opérateur de surjection.
		 * \param [inout] os (std::ostream&) : Flux dans lequel seront écrites les informations de l'instance courante.
		 * \param [inout] src (Jeu const& src) : Instance dont on doit écrire les informations.
		 * \details Cette surcharge permet d'écrire les informations de l'instance courante dans un flux de sortie.
		 * \return Flux écrit (std::ostream&).
		 * \callergraph
		 * \callgraph
		 */
		friend std::ostream& operator<<(std::ostream& os, Jeu const& src);
		
		/*! \brief Surcharge de l'opérateur d'injection.
		 * \param [inout] is (std::istream&) : Flux depuis lequel seront lues les informations de l'instance courante.
		 * \param [inout] src (Jeu& src) : Instance dont on doit lire les informations.
		 * \details Cette surcharge permet de lire les informations de l'instance courante depuis un flux d'entrée.
		 * \return Flux lu (std::istream&).
		 * \callergraph
		 * \callgraph
		 */
		friend std::istream& operator>>(std::istream& is, Jeu& src);
	
	private:
		friend class Case;
		friend class Grille;
		uint m_tour;
		uint m_yIncrementTexte;
		Grille m_grille;
	
	protected:
		/*! \brief Écrit l'instance courante.
		 * \param [inout] os (std::ostream&) : Flux dans lequel seront écrites les informations de l'instance courante.
		 * \details Cette méthode permet d'écrire les informations de l'instance courante dans un flux de sortie.
		 * \callergraph
		 * \callgraph
		 */
		virtual void ecrire(std::ostream& os) const;
		
		/*! \brief Lit l'instance courante.
		 * \param [inout] is (std::istream&) : Flux depuis lequel seront lues les informations de l'instance courante.
		 * \details Cette méthode permet de lire les informations de l'instance courante depuis un flux d'entrée.
		 * \callergraph
		 * \callgraph
		 */
		virtual void lire(std::istream& is);
};

#endif // DEF_JEU