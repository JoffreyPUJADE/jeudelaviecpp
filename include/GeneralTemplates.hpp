#ifndef GENERALTEMPLATES_HPP
#define GENERALTEMPLATES_HPP

#include <sstream>
#include <string>
#include <cstdlib>
#include <ctime>

#define INITIALISATION_ALEATOIRE srand(time(NULL));

template<typename T>
T nombreAleatoire(T borneMin, T borneMax)
{
	return ((rand() % (borneMax - borneMin + 1)) + borneMin);
}

template<typename T>
std::string T2str(T value)
{
	std::ostringstream oss;
	
	oss << value;
	
	return oss.str();
}

#endif // GENERALTEMPLATES_HPP