#ifndef DEF_CASE
#define DEF_CASE

#include "Couleur.hpp"
#include <ostream>
#include <istream>
#include <SFML/Graphics.hpp>

typedef unsigned int uint;

class Case
{
	public:
		Case(int x, int y, uint longueur, uint largeur, Couleur couleur = Couleur::Blanc);
		Case(std::istream& is);
		Case(Case const& src);
		~Case();
		
		/*! \brief Surcharge de l'opérateur d'affectation.
		 * \param [inout] src (Case const&) : Instance à recopier.
		 * \details Cette surcharge de l'opérateur d'affectation permet d'affecter de manière "sécurisée" les caractéristiques
		 * 			de l'instance passée en paramètre à l'instance courante.
		 * \return Référence sur l'instance courante (Case&).
		 * \callergraph
		 * \callgraph
		 */
		Case& operator=(Case const& src);
		
		inline int getX() const { return m_x; }
		inline int getY() const { return m_y; }
		inline uint getLongueur() const { return m_longueur; }
		inline uint getLargeur() const { return m_largeur; }
		inline bool getEtatActuel() const { return m_actuel; }
		inline bool getEtatSuivant() const { return m_suivant; }
		inline Couleur getCouleur() const { return m_couleur; }
		inline Couleur getCouleurBordure() const { return m_couleurBordure; }
		
		inline void setX(int x) { m_x = x; }
		inline void setY(int y) { m_y = y; }
		inline void setXY(int x, int y) { setX(x); setY(y); }
		inline void setLongueur(uint longueur) { m_longueur = longueur; }
		inline void setLargeur(uint largeur) { m_largeur = largeur; }
		inline void setEtatActuel(bool actuel) { m_actuel = actuel; m_couleur = m_actuel ? Couleur::Noir : Couleur::Blanc; }
		inline void setEtatSuivant(bool suivant) { m_suivant = suivant; }
		inline void setCouleur(Couleur couleur) { m_couleur = couleur; m_actuel = m_couleur == Couleur::Noir; }
		inline void setCouleurBordure(Couleur couleurBordure) { m_couleurBordure = couleurBordure; }
		
		inline void moveToX(int x) { m_x += x; }
		inline void moveToY(int y) { m_y += y; }
		inline void moveTo(int x, int y) { moveToX(x); moveToY(y); }
		void dessiner(sf::RenderWindow& fenetre) const;
		
		/*! \brief Surcharge de l'opérateur de surjection.
		 * \param [inout] os (std::ostream&) : Flux dans lequel seront écrites les informations de l'instance courante.
		 * \param [inout] src (Case const& src) : Instance dont on doit écrire les informations.
		 * \details Cette surcharge permet d'écrire les informations de l'instance courante dans un flux de sortie.
		 * \return Flux écrit (std::ostream&).
		 * \callergraph
		 * \callgraph
		 */
		friend std::ostream& operator<<(std::ostream& os, Case const& src);
		
		/*! \brief Surcharge de l'opérateur d'injection.
		 * \param [inout] is (std::istream&) : Flux depuis lequel seront lues les informations de l'instance courante.
		 * \param [inout] src (Case& src) : Instance dont on doit lire les informations.
		 * \details Cette surcharge permet de lire les informations de l'instance courante depuis un flux d'entrée.
		 * \return Flux lu (std::istream&).
		 * \callergraph
		 * \callgraph
		 */
		friend std::istream& operator>>(std::istream& is, Case& src);
	
	private:
		int m_x;
		int m_y;
		uint m_longueur;
		uint m_largeur;
		bool m_actuel;
		bool m_suivant;
		Couleur m_couleur;
		Couleur m_couleurBordure;
	
	protected:
		/*! \brief Écrit l'instance courante.
		 * \param [inout] os (std::ostream&) : Flux dans lequel seront écrites les informations de l'instance courante.
		 * \details Cette méthode permet d'écrire les informations de l'instance courante dans un flux de sortie.
		 * \callergraph
		 * \callgraph
		 */
		virtual void ecrire(std::ostream& os) const;
		
		/*! \brief Lit l'instance courante.
		 * \param [inout] is (std::istream&) : Flux depuis lequel seront lues les informations de l'instance courante.
		 * \details Cette méthode permet de lire les informations de l'instance courante depuis un flux d'entrée.
		 * \callergraph
		 * \callgraph
		 */
		virtual void lire(std::istream& is);
};

#endif // DEF_CASE