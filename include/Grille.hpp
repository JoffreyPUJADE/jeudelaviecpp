#ifndef DEF_GRILLE
#define DEF_GRILLE

#include "Case.hpp"
#include <ostream>
#include <istream>
#include <string>
#include <vector>
#include <SFML/Graphics.hpp>

typedef unsigned int uint;
typedef long long int llint;

struct Position
{
	size_t x;
	size_t y;
};

typedef struct Position Position;

class Grille
{
	public:
		Grille(uint taille, std::vector<std::string> tabConfig);
		Grille(std::istream& is, bool config = false);
		Grille(Grille const& src);
		~Grille();
		
		/*! \brief Surcharge de l'opérateur d'affectation.
		 * \param [inout] src (Grille const&) : Instance à recopier.
		 * \details Cette surcharge de l'opérateur d'affectation permet d'affecter de manière "sécurisée" les caractéristiques
		 * 			de l'instance passée en paramètre à l'instance courante.
		 * \return Référence sur l'instance courante (Grille&).
		 * \callergraph
		 * \callgraph
		 */
		Grille& operator=(Grille const& src);
		Case operator[](size_t indice) const;
		Case& operator[](size_t indice);
		
		Case operator()(size_t x, size_t y) const;
		Case& operator()(size_t x, size_t y);
		
		inline uint getTaille() const { return m_taille; }
		inline uint getLongueurCase() const { return m_longueurCase; }
		inline uint getLargeurCase() const { return m_largeurCase; }
		inline uint getLongueur() const { return (getLongueurCase() * getTaille()); }
		inline uint getLargeur() const { return (getLargeurCase() * getTaille()); }
		inline int getX() const { return m_cases[0].getX(); }
		inline int getY() const { return m_cases[0].getY(); }
		
		void setLongueurCase(uint longueurCase);
		void setLargeurCase(uint largeurCase);
		void setX(int x);
		void setY(int y);
		inline void setXY(int x, int y) { setX(x); setY(y); }
		
		std::vector<Case> voisins(size_t x, size_t y) const;
		void dessiner(sf::RenderWindow& fenetre) const;
		std::vector<Position> indicesVoisins(size_t x, size_t y);
		void testVoisins();
		
		
		/*! \brief Surcharge de l'opérateur de surjection.
		 * \param [inout] os (std::ostream&) : Flux dans lequel seront écrites les informations de l'instance courante.
		 * \param [inout] src (Grille const& src) : Instance dont on doit écrire les informations.
		 * \details Cette surcharge permet d'écrire les informations de l'instance courante dans un flux de sortie.
		 * \return Flux écrit (std::ostream&).
		 * \callergraph
		 * \callgraph
		 */
		friend std::ostream& operator<<(std::ostream& os, Grille const& src);
		
		/*! \brief Surcharge de l'opérateur d'injection.
		 * \param [inout] is (std::istream&) : Flux depuis lequel seront lues les informations de l'instance courante.
		 * \param [inout] src (Grille& src) : Instance dont on doit lire les informations.
		 * \details Cette surcharge permet de lire les informations de l'instance courante depuis un flux d'entrée.
		 * \return Flux lu (std::istream&).
		 * \callergraph
		 * \callgraph
		 */
		friend std::istream& operator>>(std::istream& is, Grille& src);
	
	private:
		friend class Case;
		uint m_taille;
		uint m_longueurCase;
		uint m_largeurCase;
		std::vector<Case> m_cases;
	
	protected:
		/*! \brief Écrit l'instance courante.
		 * \param [inout] os (std::ostream&) : Flux dans lequel seront écrites les informations de l'instance courante.
		 * \details Cette méthode permet d'écrire les informations de l'instance courante dans un flux de sortie.
		 * \callergraph
		 * \callgraph
		 */
		virtual void ecrire(std::ostream& os) const;
		
		/*! \brief Lit l'instance courante.
		 * \param [inout] is (std::istream&) : Flux depuis lequel seront lues les informations de l'instance courante.
		 * \details Cette méthode permet de lire les informations de l'instance courante depuis un flux d'entrée.
		 * \callergraph
		 * \callgraph
		 */
		virtual void lire(std::istream& is);
};

#endif // DEF_GRILLE