#ifndef DEF_COULEUR
#define DEF_COULEUR

#include <ostream>
#include <istream>
#include <string>
#include <SFML/Graphics.hpp>

/*! \struct HSL
 * \brief Structure d'une couleur HSL.
 * \details Cette structure définit une couleur HSL, avec les taux de Teinte (Hue), de Saturation
 * 			et de Luminance (Lightness).
 */
struct HSL
{
	float H; /*!< Cette variable contient le taux de Teinte (Hue). */
	float S; /*!< Cette variable contient le taux de Saturation. */
	float L; /*!< Cette variable contient le taux de Luminance (Lightness). */
	
	/*! \brief Constructeur de la structure.
	 * \param [in] _H (float) : Taux de Teinte (Hue).
	 * \param [in] _S (float) : Taux de Saturation.
	 * \param [in] _L (float) : Taux de Luminance (Lightness).
	 * \callergraph
	 * \callgraph
	 */
	HSL(float _H, float _S, float _L)
	{
		H = _H;
		S = _S;
		L = _L;
	}
};

/*! \typedef struct HSL HSL
 * \brief Alias de structure.
 * \details Cet alias définit le type "HSL" à utiliser en lieu et place du type "struct HSL".
 */
typedef struct HSL HSL;

/*! \class Couleur
 * \brief Représente une couleur.
 * \details Cette classe représente une couleur, avec ses attributs rouge, vert et bleu.
 */
class Couleur
{
	public:
		/*! \brief Constructeur par défaut de la classe.
		 * \callergraph
		 * \callgraph
		 */
		Couleur();
		
		/*! \brief Constructeur de la classe.
		 * \param [in] r (int) : Taux de rouge.
		 * \param [in] g (int) : Taux de vert.
		 * \param [in] b (int) : Taux de bleu.
		 * \param [in] alpha (int) : Taux de transparence.
		 * \callergraph
		 * \callgraph
		 */
		Couleur(int r, int g, int b, int alpha);
		
		/*! \brief Constructeur de la classe.
		 * \param [in] H (float) : Taux de Teinte (Hue).
		 * \param [in] S (float) : Taux de Saturation.
		 * \param [in] L (float) : Taux de Luminance (Lightness).
		 * \callergraph
		 * \callgraph
		 */
		Couleur(float H, float S, float L);
		
		/*! \brief Constructeur de la classe.
		 * \param [in] hsl (HSL) : Instance de la structure ::HSL.
		 * \callergraph
		 * \callgraph
		 */
		Couleur(HSL hsl);
		
		/*! \brief Constructeur de la classe.
		 * \param [in] hexColor (std::string) : Valeur hexadécimale.
		 * \callergraph
		 * \callgraph
		 */
		Couleur(std::string hexColor);
		
		/*! \brief Constructeur de la classe.
		 * \param [in] is (std::istream&) : Flux d'entrée.
		 * \callergraph
		 * \callgraph
		 */
		Couleur(std::istream& is);
		
		/*! \brief Constructeur de copie de la classe.
		 * \param [inout] src (Couleur const&) : Instance à recopier.
		 * \callergraph
		 * \callgraph
		 */
		Couleur(Couleur const& src);
		
		/*! \brief Destructeur de la classe.
		 */
		~Couleur();
		
		
		/*! \brief Surcharge de l'opérateur d'affectation.
		 * \param [inout] src (Couleur const&) : Instance à recopier.
		 * \details Cette surcharge de l'opérateur d'affectation permet d'affecter de manière "sécurisée" les caractéristiques
		 * 			de l'instance passée en paramètre à l'instance courante.
		 * \return Référence sur l'instance courante (Couleur&).
		 * \callergraph
		 * \callgraph
		 */
		Couleur& operator=(Couleur const& src);
		
		
		/*! \brief Accesseur de la classe.
		 * \details Cette méthode permet d'obtenir le taux de rouge de la couleur.
		 * \return Taux de rouge (int).
		 * \callergraph
		 * \callgraph
		 */
		inline int getRouge() const { return m_rouge; }
		
		/*! \brief Accesseur de la classe.
		 * \details Cette méthode permet d'obtenir le taux de vert de la couleur.
		 * \return Taux de vert (int).
		 * \callergraph
		 * \callgraph
		 */
		inline int getVert() const { return m_vert; }
		
		/*! \brief Accesseur de la classe.
		 * \details Cette méthode permet d'obtenir le taux de bleu de la couleur.
		 * \return Taux de bleu (int).
		 * \callergraph
		 * \callgraph
		 */
		inline int getBleu() const { return m_bleu; }
		
		/*! \brief Accesseur de la classe.
		 * \details Cette méthode permet d'obtenir le taux de transparence de la couleur.
		 * \return Taux de transparence (int).
		 * \callergraph
		 * \callgraph
		 */
		inline int getTransparent() const { return m_transparent; }
		
		
		/*! \brief Mutateur de la classe.
		 * \param [in] rouge (int) : Nouveau taux de rouge.
		 * \details Cette méthode permet de modifier le taux de rouge de la couleur.
		 * \callergraph
		 * \callgraph
		 */
		inline void setRouge(int rouge) { m_rouge = rouge; }
		
		/*! \brief Mutateur de la classe.
		 * \param [in] vert (int) : Nouveau taux de vert.
		 * \details Cette méthode permet de modifier le taux de vert de la couleur.
		 * \callergraph
		 * \callgraph
		 */
		inline void setVert(int vert) { m_vert = vert; }
		
		/*! \brief Mutateur de la classe.
		 * \param [in] bleu (int) : Nouveau taux de bleu.
		 * \details Cette méthode permet de modifier le taux de bleu de la couleur.
		 * \callergraph
		 * \callgraph
		 */
		inline void setBleu(int bleu) { m_bleu = bleu; }
		
		/*! \brief Mutateur de la classe.
		 * \param [in] transparent (int) : Nouveau taux de transparence.
		 * \details Cette méthode permet de modifier le taux de transparence de la couleur.
		 * \callergraph
		 * \callgraph
		 */
		inline void setTransparent(int transparent) { m_transparent = transparent; }
		
		
		// Formules de conversion prises sur le site : https://www.rapidtables.com/convert/color/rgb-to-hsl.html
		/*! \brief Converti la couleur.
		 * \details Cette méthode permet de convertir la couleur courante (définit par des valeurs RGB)
		 * 			en des valeurs HSL.
		 * \return Couleur convertie (HSL).
		 * \callergraph
		 * \callgraph
		 */
		HSL toHSL() const;
		
		// Formules de conversion prises sur le site : https://www.rapidtables.com/convert/color/rgb-to-hex.html
		/*! \brief Converti la couleur.
		 * \details Cette méthode permet de convertir la couleur courante (définit par des valeurs RGB)
		 * 			en une valeur hexadécimale.
		 * \return Couleur convertie (std::string).
		 * \callergraph
		 * \callgraph
		 */
		std::string toHex() const;
		
		/*! \brief Converti la couleur.
		 * \details Cette méthode permet de convertir la couleur courante (définit par des valeurs RGB)
		 * 			en une couleur du type définit par la SFML.
		 * \return Couleur convertie (sf::Color).
		 * \callergraph
		 * \callgraph
		 */
		inline sf::Color toSFML() const { return sf::Color(m_rouge, m_vert, m_bleu, m_transparent); }
		
		
		/*! \brief Surcharge de l'opérateur de surjection.
		 * \param [inout] os (std::ostream&) : Flux dans lequel seront écrites les informations de l'instance courante.
		 * \param [inout] src (Couleur const& src) : Instance dont on doit écrire les informations.
		 * \details Cette surcharge permet d'écrire les informations de l'instance courante dans un flux de sortie.
		 * \return Flux écrit (std::ostream&).
		 * \callergraph
		 * \callgraph
		 */
		friend std::ostream& operator<<(std::ostream& os, Couleur const& src);
		
		/*! \brief Surcharge de l'opérateur d'injection.
		 * \param [inout] is (std::istream&) : Flux depuis lequel seront lues les informations de l'instance courante.
		 * \param [inout] src (Couleur& src) : Instance dont on doit lire les informations.
		 * \details Cette surcharge permet de lire les informations de l'instance courante depuis un flux d'entrée.
		 * \return Flux lu (std::istream&).
		 * \callergraph
		 * \callgraph
		 */
		friend std::istream& operator>>(std::istream& is, Couleur& src);
		
		/*! \brief Surcharge de l'opérateur d'égalité.
		 * \param [inout] r1 (Couleur const&) : Première couleur à comparer.
		 * \param [inout] r2 (Couleur const&) : Seconde couleur à comparer.
		 * \details Cette surcharge permet de comparer deux couleurs afin de vérifier leur égalité.
		 * \return Résultat de la vérification (bool).
		 * \callergraph
		 * \callgraph
		 */
		friend bool operator==(Couleur const& src1, Couleur const& src2);
		
		/*! \brief Surcharge de l'opérateur d'inégalité.
		 * \param [inout] r1 (Couleur const&) : Première couleur à comparer.
		 * \param [inout] r2 (Couleur const&) : Seconde route à comparer.
		 * \details Cette surcharge permet de comparer deux couleurs afin de vérifier leur inégalité.
		 * \return Résultat de la vérification (bool).
		 * \callergraph
		 * \callgraph
		 */
		friend bool operator!=(Couleur const& src1, Couleur const& src2);
		
		static Couleur Rouge; /*!< Cet attribut statique représente la couleur rouge. */
		static Couleur Vert; /*!< Cet attribut statique représente la couleur verte. */
		static Couleur Bleu; /*!< Cet attribut statique représente la couleur bleue. */
		static Couleur Jaune; /*!< Cet attribut statique représente la couleur jaune. */
		static Couleur Magenta; /*!< Cet attribut statique représente la couleur magenta. */
		static Couleur Cyan; /*!< Cet attribut statique représente la couleur cyan. */
		static Couleur Noir; /*!< Cet attribut statique représente la couleur noire. */
		static Couleur Blanc; /*!< Cet attribut statique représente la couleur blanche. */
		static Couleur Transparent; /*!< Cet attribut statique représente la couleur transparente. */
	
	private:
		int m_rouge; /*!< Cet attribut contient le taux de rouge. */
		int m_vert; /*!< Cet attribut contient le taux de vert. */
		int m_bleu; /*!< Cet attribut contient le taux de bleu. */
		int m_transparent; /*!< Cet attribut contient le taux de transparence. */
	
	protected:
		/*! \brief Écrit l'instance courante.
		 * \param [inout] os (std::ostream&) : Flux dans lequel seront écrites les informations de l'instance courante.
		 * \details Cette méthode permet d'écrire les informations de l'instance courante dans un flux de sortie.
		 * \callergraph
		 * \callgraph
		 */
		void ecrire(std::ostream& os) const;
		
		/*! \brief Lit l'instance courante.
		 * \param [inout] is (std::istream&) : Flux depuis lequel seront lues les informations de l'instance courante.
		 * \details Cette méthode permet de lire les informations de l'instance courante depuis un flux d'entrée.
		 * \callergraph
		 * \callgraph
		 */
		void lire(std::istream& is);
		
		/*! \brief Vérifie l'égalité.
		 * \param [inout] src (Couleur const&) : Instance à comparer.
		 * \details Cette méthode permet de vérifier l'égalité entre la couleur passée en paramètre et
		 * 			l'instance courante.
		 * \return Résultat de la vérification (bool).
		 * \callergraph
		 * \callgraph
		 */
		bool estEgal(Couleur const& src) const;
};

#endif // DEF_COULEUR