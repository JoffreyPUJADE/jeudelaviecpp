﻿cmake_minimum_required(VERSION 3.1)

project(JeuDeLaVieCPP)

# Macro trouvée sur le site : https://stackoverflow.com/questions/10851247/how-do-i-activate-c-11-in-cmake

macro(use_cxx11)
  if (CMAKE_VERSION VERSION_LESS "3.1")
    if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
      set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++11")
    endif ()
  else ()
    set (CMAKE_CXX_STANDARD 11)
  endif ()
endmacro(use_cxx11)

add_compile_options(
  "-Wall" "-Wextra" "-Werror" "-fexceptions"
  "$<$<CONFIG:DEBUG>:-O3;-g;-ggdb>"
)

if(WIN32)
	add_compile_options(
		"-limagehlp" "-lkernel32" "-ldbghelp"
	)
	
	if(MINGW)
		add_link_options(
			"LINKER:-limagehlp" "LINKER:-lkernel32" "LINKER:-ldbghelp"
		)
	endif(MINGW)
	
	if(MSVC)
		add_link_options(
			"LINKER:imagehlp" "LINKER:kernel32" "LINKER:dbghelp"
		)
	endif(MSVC)
endif(WIN32)

if(UNIX)
	#do something
endif(UNIX)

if(APPLE)
	add_compile_options("-fno-pie")
	
	add_link_options("-fno-pie")
endif(APPLE)

## If you want to link SFML statically
# set(SFML_STATIC_LIBRARIES TRUE)

## In most cases better set in the CMake cache
# set(SFML_DIR "<sfml root prefix>/lib/cmake/SFML")

find_package(
	SFML 2.5
	COMPONENTS audio graphics system REQUIRED
)

# Pour trouver de manière récursive tous les fichiers sources et tous les fichiers d'en-tête.
file(GLOB_RECURSE SOURCES RELATIVE ${CMAKE_SOURCE_DIR} "src/*.cpp")
file(GLOB_RECURSE HEADERS RELATIVE ${CMAKE_SOURCE_DIR} "include/*.hpp")

add_executable(
	JeuDeLaVieCPP
	${HEADERS}
	${SOURCES}
)

target_link_libraries(
	JeuDeLaVieCPP
	sfml-audio
	sfml-graphics
	sfml-system
)
